package app.algo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ImmutableReversedArrayList<T> extends ArrayList<T> {

    public ImmutableReversedArrayList(List<T> originalList) {
        super(originalList);
    }

    @Override
    public Iterator<T> iterator() {
        final ListIterator<T> i = this.listIterator(this.size());

        return new Iterator<T>() {
            public boolean hasNext() { return i.hasPrevious(); }
            public T next() { return i.previous(); }
            public void remove() { i.remove(); }
        };
    }

    @Override
    public T get(int index) {
        return super.get((this.size() - 1) - index);
    }

    @Override
    public T set(int index, T element) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public static <T> ReversedIterable<T> reversed(List<T> originalList) {
        return new ReversedIterable<>(originalList);
    }
}
