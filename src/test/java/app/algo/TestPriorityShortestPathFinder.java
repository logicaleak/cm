package app.algo;

import app.CmNode;
import app.MapReader;
import app.graph.DirectedGraph;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ozum on 10.07.2016.
 */
public class TestPriorityShortestPathFinder {

    @Test
    public void Should_Successfully_Calculate_Shortest_Path() throws Exception {
        DirectedGraph map = MapReader.startRead("/data.dat").read().getGraph();
        PriorityShortestPathFinder priorityShortestPathFinder = new PriorityShortestPathFinder(map);
        PriorityShortestPathFinder.ShortestPath shortestPath =
                priorityShortestPathFinder.calculateShortestPath(new CmNode("2515715574"), new CmNode("2515715577"));

        Assert.assertEquals(Integer.valueOf(38), shortestPath.totalCost);
        Assert.assertEquals(new CmNode("2515715574"), shortestPath.path.get(0));
        Assert.assertEquals(new CmNode("2515715575"), shortestPath.path.get(1));
        Assert.assertEquals(new CmNode("2515715576"), shortestPath.path.get(2));
        Assert.assertEquals(new CmNode("2515715577"), shortestPath.path.get(3));
    }

    @Test
    public void Should_Successfully_Calculate_Shortest_Path_Case_2() throws Exception {
        DirectedGraph map = MapReader.startRead("/data.dat").read().getGraph();
        PriorityShortestPathFinder priorityShortestPathFinder = new PriorityShortestPathFinder(map);
        PriorityShortestPathFinder.ShortestPath shortestPath =
                priorityShortestPathFinder.calculateShortestPath(new CmNode("315279162"), new CmNode("313997407"));
        Assert.assertEquals(Integer.valueOf(348), shortestPath.totalCost);
        Assert.assertEquals(new CmNode("315279162"), shortestPath.path.get(0));
        Assert.assertEquals(new CmNode("315279163"), shortestPath.path.get(1));
        Assert.assertEquals(new CmNode("315279164"), shortestPath.path.get(2));
        Assert.assertEquals(new CmNode("315279165"), shortestPath.path.get(3));
        Assert.assertEquals(new CmNode("315279166"), shortestPath.path.get(4));
        Assert.assertEquals(new CmNode("315279167"), shortestPath.path.get(5));
        Assert.assertEquals(new CmNode("315279168"), shortestPath.path.get(6));
        Assert.assertEquals(new CmNode("315279169"), shortestPath.path.get(7));
        Assert.assertEquals(new CmNode("315279170"), shortestPath.path.get(8));
        Assert.assertEquals(new CmNode("315279171"), shortestPath.path.get(9));
        Assert.assertEquals(new CmNode("315279172"), shortestPath.path.get(10));
        Assert.assertEquals(new CmNode("313997407"), shortestPath.path.get(11));
    }
}
